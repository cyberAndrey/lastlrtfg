package com.company;

public class Token {
    private final Type type;
    private final String context;
    private final int index;

    public Token(Type type, String context, int index) {
        this.type = type;
        this.context = context;
        this.index = index;
    }

    public Type getType() {
        return type;
    }

    public String getContext() {
        return context;
    }

    public int getIndex() {
        return index;
    }


    @Override
    public String toString() {
        return type + " " + context + " " + index;
    }

    @Override
    public boolean equals(Object obj) {
        Token token;
        if (obj instanceof Token)
            token = (Token) obj;
        else return false;
        return this.type.equals(token.getType()) && this.context.equals(token.getContext()) && this.index == token.getIndex();
    }
}
