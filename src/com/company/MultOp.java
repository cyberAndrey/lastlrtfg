package com.company;

public class MultOp extends Operator {
    public MultOp() {
        super("*", 2, 2);
    }

    @Override
    public double result(double[] args) {
        return args[0]*args[1];
    }
}
