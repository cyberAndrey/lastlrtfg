package com.company;

public class MinusOp extends Operator {

    public MinusOp() {
        super("-", 2, 3);
    }

    @Override
    public double result(double[] args) {
        return args[1] - args[0];
    }
}
