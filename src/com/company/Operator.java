package com.company;

public abstract class Operator {
    //Значение
    private final String value;
    //Количество аргументов
    private final int countArgs;
    //Приоритет
    private final int priority;

    public Operator(String value, int countArgs, int priority) {
        this.value = value;
        this.countArgs = countArgs;
        this.priority = priority;
    }

    public String getValue() {
        return value;
    }

    public int getPriority() {
        return priority;
    }


//    public double result(String value, double a, double b) {
//        return value.equals("+") ? sum(a, b) : value.equals("-") ? subtraction(a, b) : value.equals("*") ? multiply(a, b) :
//                value.equals("/") ? division(a, b) : null;
//    }
//
//    static double sum(double a, double b) {
//        return a + b;
//    }
//
//    private static double subtraction(double a, double b) {
//        return a - b;
//    }
//
//    private static double multiply(double a, double b) {
//        return a * b;
//    }
//
//    private static double division(double a, double b) {
//        return a / b;
//    }
    public abstract double result(double[] args);

    @Override
    public String toString() {
        return super.toString() + " " + countArgs + " " + priority;
    }

}
