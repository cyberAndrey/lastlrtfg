package com.company;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class Main {
    static List<Operator> operators = new ArrayList<>();
    static {
        operators.add(new PlusOp());
        operators.add(new MinusOp());
        operators.add(new MultOp());
        operators.add(new DevOp());
    }

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("Введите строку:\nВозможные операторы: + - * / ++N --N\nТакже доступны круглые скобки ( )");
            String context = reader.readLine();
            if (context.equals("")) break;
            Automat automat = new Automat(operators);
            List<Token> list = automat.tokenization(context);
            list = automat.preprocessing(list);
            Queue<Token> postfix = automat.postfixTransition(list);
            Token res = automat.calculate(postfix);
            System.out.printf("%.3f%n", Double.parseDouble(res.getContext()));
        }
        reader.close();
    }
}
