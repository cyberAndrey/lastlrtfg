package com.company;

public class DevOp extends Operator {
    public DevOp() {
        super("/", 2, 2);
    }

    @Override
    public double result(double[] args) {
        return args[0] / args[1];
    }
}
