package com.company;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Automat {
    List<Operator> operators;
    public Automat(List<Operator> operators) {
        this.operators = operators;
    }

    /**
     * Шаг 1. Токенизация
     * @param str Входная строка
     * @return Последовательность токенов
     */
    public List<Token> tokenization(String str) throws Exception {
        int index = 0;
        //Для распознования чисел
        Pattern pattern = Pattern.compile("^\\d+([.,]\\d+)?");
        Set<String> set = new HashSet<>();
        for (Operator o : operators) set.add(o.getValue());
        List<Token> tokens = new ArrayList<>();
        char[] arrayChar = str.toCharArray();
        //Пока строка не кончится
        while (index < arrayChar.length){
            //Проверка на пробел
            if (arrayChar[index] == ' ' || arrayChar[index] == '\t') { index++; }
            //Проверка на скобки
            else if (arrayChar[index] == '(' || arrayChar[index] == ')') {
                Type type = arrayChar[index] == '(' ? Type.OPEN : Type.CLOSE;
                tokens.add(new Token(type, Character.toString(arrayChar[index]), index));
                index++; }
            //Проверка на оператор (может быть длиной = 2)
            else if (set.contains(Character.toString(arrayChar[index]))) {
                String operator;
                if (index+1 < arrayChar.length && set.contains(Character.toString(arrayChar[index+1]))) {
                    operator = str.substring(index, index + 2);
                } else {
                    operator = str.substring(index, index + 1);
                }
                boolean flag = false;
                for (Operator o : operators)
                    if (o.getValue().equals(operator)) {
                        flag = true;
                        break;
                    }
                if (!flag)
                    throw new Exception("Недопустимый оператор");
                tokens.add(new Token(Type.OPERATOR, operator, index));
                index += operator.length();
            }
            //Проверка на число
            else {
                    Matcher matcher = pattern.matcher(str.substring(index));
                    if (matcher.find()) {
                        String num = str.substring(index, index + matcher.end());
                        tokens.add(new Token(Type.NUMBER, num, index));
                        index += num.length();
                    } else throw new Exception("Ошибка в токенизации. Не удается распознать строку");
            }

        }
        return tokens;
    }

    public List<Token> preprocessing(List<Token> tokens) throws Exception {
        //Проверяю, чтобы операторы были
        boolean thereIsOperators = false;
        for (Token token : tokens)
            if (token.getType().equals(Type.OPERATOR))
            { thereIsOperators = true; break; }
        if (!thereIsOperators) throw new Exception("Отсутствуют операторы");
        //Проверка на корректность скобок
        if (!checkBrackets(tokens))
            throw new Exception("Некорректно проставлены скобки.");
        for (int i = 0; i < tokens.size()-1; i++) {
            //Вставляю умножение
            if (tokens.get(i+1).getType().equals(Type.OPEN) &&tokens.get(i).getType().equals(Type.NUMBER))
                tokens.add(i+1, new Token(Type.OPERATOR, "*", -1));
        }
        //Проверяю, чтобы не было 2 оператора подряд
        Token first = tokens.get(0);
        for (Token token : tokens) {
            if (token.equals(first)) { continue; }
        if (token.getType().equals(Type.OPERATOR) && first.getType().equals(Type.OPERATOR))
            throw new Exception("Ошибка. Два оператора подряд.");
        first = token;
        }
        return tokens;
    }

    public Queue<Token> postfixTransition(List<Token> tokens) {
        Stack<Token> stack = new Stack<>();
        Queue<Token> queue = new ArrayDeque<>();
        for (Token token : tokens) {
            if (token.getType().equals(Type.NUMBER)) {
                queue.add(token);
            } else if (token.getType().equals(Type.OPERATOR)) {

                if (stack.isEmpty() || stack.peek().getType().equals(Type.OPEN)) {
                    stack.push(token);
                } else if (checkPriority(token, stack.peek())) {
                    stack.push(token);
                } else {
                    while (true) {
                        queue.add(stack.pop());
                        if (stack.size()==0) break;
                        if ((stack.peek().getType().equals(Type.OPERATOR) && checkLessPriority(token, stack.peek())) ||
                        stack.peek().getType().equals(Type.OPEN)) break;
                    }
                    stack.push(token);
                }

            } else if (token.getType().equals(Type.OPEN)) {
                stack.push(token);
            } else if (token.getType().equals(Type.CLOSE)) {
                Token top;
                while (true) {
                    top = stack.pop();
                    if (top.getType().equals(Type.OPEN)) break;
                    else queue.add(top);
                }
            }
        }
        while (!stack.isEmpty())
            queue.add(stack.pop());
        return queue;
    }

    public Token calculate(Queue<Token> tokens) {
        Stack<Token> stack = new Stack<>();
        for (Token token : tokens) {
            if (token.getType().equals(Type.NUMBER))
                stack.push(token);
            else {
                double[] pars = new double[2];
                pars[0] = Double.parseDouble(stack.pop().getContext());
                pars[1] = Double.parseDouble(stack.pop().getContext());
                Operator operator = null;
                for (Operator o : operators)
                    if (o.getValue().equals(token.getContext())) {
                        operator = o;
                        break;
                    }
                String res = Double.toString(operator.result(pars));
                stack.push(new Token(Type.NUMBER, res, 0));
            }
        }
        return stack.pop();
    }

    private boolean checkPriority(Token source, Token inStack) {
        int one = 0, two = 0;
        for (Operator operator : operators) {
            if (operator.getValue().equals(source.getContext()))
                one = operator.getPriority();
            if (operator.getValue().equals(inStack.getContext()))
                two = operator.getPriority();
        }
        return  one < two;
    }

    private boolean checkLessPriority(Token source, Token inStack) {
        int one = 0, two = 0;
        for (Operator operator : operators) {
            if (operator.getValue().equals(source.getContext()))
                one = operator.getPriority();
            if (operator.getValue().equals(inStack.getContext()))
                two = operator.getPriority();
        }
        return  two > one;
    }

    private boolean checkBrackets(List<Token> tokens) {
        Stack<Token> all = new Stack<>();
        for (Token token : tokens) {
            if (token.getType().equals(Type.OPEN))
                all.push(token);
            if (token.getType().equals(Type.CLOSE))
                all.pop();
        }
        return all.size() == 0;
    }



}
