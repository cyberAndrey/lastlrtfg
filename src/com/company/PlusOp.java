package com.company;

public class PlusOp extends Operator {

    public PlusOp() {
        super("+", 2, 3);
    }
    @Override
    public double result(double[] args) {
        return args[0] + args[1];
    }
}
